Feature: ainoniwa.net DNS service behavior

  Scenario Outline: tcp port access
    When access host <ip_version>: <host>:<port>
    Then tcp connection is valid
    
    Examples: Ipv4
      | host             | port | ip_version |
      | ns1.ainoniwa.net | 53   | ipv4       |

#    Examples: Ipv6
#      | host                    | port | ip_version |
#      | 2400:406f:1d6e:fb00::51 | 53   | ipv6       |
#      | ns2.ainoniwa.net        | 53   | ipv6       |

  Scenario Outline: ainoniwa.net lookup
    When udp query <fqdn> <record> to <resolver>
    Then answer value is <value>
    When tcp query <fqdn> <record> to <resolver>
    Then answer value is <value>
    
    Examples: IPv4
      | resolver     | fqdn             | record | value                      |
      | 36.3.114.176 | ainoniwa.net     | NS     | ns1.ainoniwa.net           |
      | 36.3.114.176 | ainoniwa.net     | NS     | ns2.ainoniwa.net           |
      | 36.3.114.176 | ainoniwa.net     | MX     | 1 aspmx.l.google.com.      |
      | 36.3.114.176 | ainoniwa.net     | MX     | 5 alt1.aspmx.l.google.com. |
      | 36.3.114.176 | ainoniwa.net     | MX     | 5 alt2.aspmx.l.google.com. |
      | 36.3.114.176 | ainoniwa.net     | MX     | 10 aspmx2.googlemail.com.  |
      | 36.3.114.176 | ainoniwa.net     | MX     | 10 aspmx3.googlemail.com.  |
      | 36.3.114.176 | www.ainoniwa.net | A      | 36.3.114.176               |
      | 36.3.114.176 | www.ainoniwa.net | AAAA   | 2400:406f:1d6e:fb00::1:80  |
      | 36.3.114.176 | ns1.ainoniwa.net | A      | 36.3.114.176               |
      | 36.3.114.176 | ns2.ainoniwa.net | AAAA   | 2400:406f:1d6e:fb00::51    |

#    Examples: IPv6
#      | resolver                | fqdn             | record | value                      |
#      | 2400:406f:1d6e:fb00::51 | ainoniwa.net     | NS     | ns1.ainoniwa.net           |
#      | 2400:406f:1d6e:fb00::51 | ainoniwa.net     | NS     | ns2.ainoniwa.net           |
#      | 2400:406f:1d6e:fb00::51 | ainoniwa.net     | MX     | 1 aspmx.l.google.com.      |
#      | 2400:406f:1d6e:fb00::51 | ainoniwa.net     | MX     | 5 alt1.aspmx.l.google.com. |
#      | 2400:406f:1d6e:fb00::51 | ainoniwa.net     | MX     | 5 alt2.aspmx.l.google.com. |
#      | 2400:406f:1d6e:fb00::51 | ainoniwa.net     | MX     | 10 aspmx2.googlemail.com.  |
#      | 2400:406f:1d6e:fb00::51 | ainoniwa.net     | MX     | 10 aspmx3.googlemail.com.  |
#      | 2400:406f:1d6e:fb00::51 | www.ainoniwa.net | A      | 36.3.114.176               |
#      | 2400:406f:1d6e:fb00::51 | www.ainoniwa.net | AAAA   | 2400:406f:1d6e:fb00::1:80  |
#      | 2400:406f:1d6e:fb00::51 | ns1.ainoniwa.net | A      | 36.3.114.176               |
#      | 2400:406f:1d6e:fb00::51 | ns2.ainoniwa.net | AAAA   | 2400:406f:1d6e:fb00::51    |

