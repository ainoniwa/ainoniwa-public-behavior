Feature: www.ainoniwa.net web service behavior

  Scenario Outline: tcp port access
    When access host <ip_version>: <host>:<port>
    Then tcp connection is valid
    
    Examples: IPv4
      | ip_version | host             | port |
      | ipv4       | www.ainoniwa.net | 80   |
      | ipv4       | www.ainoniwa.net | 443  |

#    Examples: IPv6
#      | ip_version | host             | port |
#      | ipv6       | www.ainoniwa.net | 80   |
#      | ipv6       | www.ainoniwa.net | 443  |

  Scenario Outline: web page http access
    When access URL: <url><path>
    Then return status_code is 200

    Examples: http://www.ainoniwa.net
      | url                     | path |
      | http://www.ainoniwa.net | / |
      | http://www.ainoniwa.net | /mirror/ |
      | http://www.ainoniwa.net | /mirror/ubuntu-iso/ |
      | http://www.ainoniwa.net | /doku/ |
      | http://www.ainoniwa.net | /pdoc/ |
      | http://www.ainoniwa.net | /ssp/ |
      | http://www.ainoniwa.net | /ssp/?p=1197 |
      | http://www.ainoniwa.net | /ssp/?s=primergy |
      | http://www.ainoniwa.net | /ssp/?tag=primergy |

  Scenario Outline: web page https access
    When access URL: https://www.ainoniwa.net<path>
    Then return status_code is 200

    Examples: Access paths
      | path               |
      | /                  |
      | /piwik/            |
      | /doku/             |
      | /pdoc/             |
      | /ssp/              |
      | /ssp/?p=1197       |
      | /ssp/?s=primergy   |
      | /ssp/?tag=primergy |
      | /pelican/          |
      | /mirror/           |

