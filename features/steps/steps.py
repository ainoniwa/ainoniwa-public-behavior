#!/usr/bin/env python
# coding=utf-8
import socket
import unittest
import requests
import json
import dns.resolver
from behave import when, then
from nose.tools import eq_


@when(u'access host {ip_version}: {host}:{port:n}')
def step_impl(context, ip_version, host, port):
    if ip_version == "ipv4":
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if ip_version == "ipv6":
        client = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    try:
        client.connect((host, port))
    except:
        raise
    context.connection = True


@then(u'tcp connection is valid')
def step_impl(context):
    assert context.connection


@when(u'access URL: {url}')
def step_impl(context, url):
    response = requests.get(url)
    context.response = response


@then(u'return status_code is {status_code:n}')
def step_impl(context, status_code):
    eq_(context.response.status_code, status_code)


@when(u'udp query {fqdn} {record} to {resolver}')
def step_impl(context, fqdn, record, resolver):
    client = dns.resolver.Resolver(configure=False)
    client.nameservers = [resolver]
    context.answer = client.query(fqdn, record)


@when(u'tcp query {fqdn} {record} to {resolver}')
def step_impl(context, fqdn, record, resolver):
    client = dns.resolver.Resolver(configure=False)
    client.nameservers = [resolver]
    context.answer = client.query(fqdn, record, tcp=True)


@then(u'answer value is {value}')
def step_impl(context, value):
    for rr_item in context.answer.rrset.items:
        if rr_item.to_text().rstrip('.') == value.rstrip('.'):
            return
    assert False


@when(u'validate IPv6 DNS record {fqdn}')
def step_impl(context, fqdn):
    url = "http://ipv6-test.com/json/webaaaa.php"
    params = {
        "url": fqdn
    }
    response = requests.get(url, params)
    context.response = response.json()


@when(u'validate IPv6 {schema} accessibility {fqdn}')
def step_impl(context, fqdn, schema):
    url = "http://ipv6-test.com/json/webserver.php"
    params = {
        "url": fqdn,
        "scheme": schema.lower()
    }
    response = requests.get(url, params)
    context.response = response.json()


@when(u'validate IPv6 nameserver for {fqdn}')
def step_impl(context, fqdn):
    url = "http://ipv6-test.com/json/webdns.php"
    params = {
        "url": fqdn
    }
    response = requests.get(url, params)
    context.response = response.json()


@then(u'verify response json data')
def step_impl(context):
    unittest.TestCase().assertEqual(json.loads(context.text), context.response)
