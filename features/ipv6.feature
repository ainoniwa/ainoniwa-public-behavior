Feature: IPv6
 ainoniwa.net IPv6 accessibility

  Scenario: validate IPv6 accessibility
    When validate IPv6 DNS record www.ainoniwa.net
    Then verify response json data
    """
    {
        "dns_aaaa": "2400:406f:1d6e:fb00::1:80"
    }
    """

    When validate IPv6 HTTP accessibility www.ainoniwa.net
    Then verify response json data
    """
    {
        "dns_aaaa": "2400:406f:1d6e:fb00::1:80",
        "server": "Apache/2.4.41 (Ubuntu)",
        "title": ""
    }
    """

    When validate IPv6 HTTPS accessibility www.ainoniwa.net
    Then verify response json data
    """
    {
        "dns_aaaa": "2400:406f:1d6e:fb00::1:80",
        "server": "Apache/2.4.41 (Ubuntu)",
        "title": ""
    }
    """
